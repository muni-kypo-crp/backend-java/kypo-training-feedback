22.12-rc.1 Changed sandboxId type to string.
2.0.2 Removed stacktrace from errors.
2.0.1 Change the configuration for swagger docs generation.
2.0.0 Migrate project to Java 17.
1.1.1 Bugfix - the value training time missed in the individual commands.
1.1.0 Support creation of graphs for instances with the usage of local sandboxes. Throw an exception when cannot create a backward edge in the graph.
1.0.5 Updated dependencies to the latest versions. Code cleanup. Configured multistage docker build to decrease the resulting image size. 
1.0.4 Update log4j2 version to avoid CVE-2021-44228 vulnerability
1.0.3 Bugfix - escape command options to prevent bad graph definition.
1.0.2 Added SpringBoot Maven plugin to create executable .jar file.
1.0.1 Added Gitlab CI.
1.0.0 Initial version, integrated with KYPO CRP.
