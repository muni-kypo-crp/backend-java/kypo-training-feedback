package cz.muni.ics.kypo.training.feedback.enums;

public enum GraphType {
    REFERENCE_GRAPH,
    SUMMARY_GRAPH,
    TRAINEE_GRAPH
}
